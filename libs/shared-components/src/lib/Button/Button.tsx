import React from 'react';

import './Button.scss';

/* eslint-disable-next-line */
export interface ButtonProps {
  className?: string;
}

export const Button = (props: ButtonProps) => {
  const { className } = props;
  return (
    <button className={`btn ${className}`}>An example shared button!</button>
  );
};

export default Button;
