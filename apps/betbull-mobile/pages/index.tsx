import React from 'react';
import { Button, Card } from '@monorepo-betbull-poc/shared-components';
import { environment } from '../environments/environment';
import './index.scss';

export const Index = () => {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./${fileName}.${style} file.
   */
  return (
    <div className="app">
      <main>
        <div className="jumbotron justify-content-center d-flex">
          <h1>
            Welcome to betbull-mobile! [
            {environment.production ? 'PROD' : 'DEV'}]
          </h1>
        </div>
        <div className="row container-fluid">
          {[...Array(3).keys()].map((x, i) => (
            <div key={x} className="col-md-4">
              <Card key={i} />
            </div>
          ))}
          <div className="col-12">
            <Button className="btn-danger"></Button>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Index;
