import App from 'next/app';
import Head from 'next/head';
import React from 'react';
import { Header } from '../components';

export default class CustomApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }
  render() {
    const { Component, pageProps } = this.props
    return (
      <>
        <Head>
          <title>My Cool Monorepo App</title>
        </Head>
        <div>
          <Header></Header>
          <Component {...pageProps} />
        </div>
      </>
    );
  }
}
