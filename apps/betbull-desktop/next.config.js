const withPlugins = require('next-compose-plugins');
const withSass = require('@zeit/next-sass');
const withTM = require("next-transpile-modules")(["shared-components"]);

module.exports = withPlugins([[withTM], [withSass, {
  // Set this to true if you use CSS modules.
  // See: https://github.com/css-modules/css-modules
  cssModules: false,
  pageExtensions: ['mdx', 'jsx', 'js', 'ts', 'tsx']
}]], {});