import React from 'react';
import { Card, Button } from '@monorepo-betbull-poc/shared-components';
import { SideMenuComponent } from '../components';
import { environment } from '../environments/environment';

export const Index = () => {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./${fileName}.${style} file.
   */
  return (
    <div className="app">
      <div className="row">
        <SideMenuComponent />
        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 className="h2">
              Welcome to betbull-mobile! [
              {environment.production ? 'PROD' : 'DEV'}]
            </h1>
          </div>
          <div className="row">
            <div className="col-12">
              <Button className="btn-success"></Button>
            </div>
          </div>
          <div className="row">
            {[...Array(3).keys()].map((x, i) => (
              <div key={x} className="col-md-4">
                <Card key={i} />
              </div>
            ))}
          </div>
        </main>
      </div>
    </div>
  );
};

export default Index;
