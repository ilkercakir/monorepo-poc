import React from 'react';

import '../../pages/index.scss';

/* eslint-disable-next-line */
export interface ExampleComponentProps {
  className?: string;
  text: string;
}

export const ExampleComponent = (props: ExampleComponentProps) => {
  const { className, text } = props;
  return (
    <div className={`btn m-2 ${className}`}>
      <h1>Welcome to {text} component!</h1>
    </div>
  );
};

export default ExampleComponent;
